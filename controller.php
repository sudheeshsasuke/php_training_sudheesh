<?php
require_once 'model.php';

function admin_login() {

}

function user_registartion() {

    require 'templates/user_registartion.tpl.php';
}

function register_action() {

    register_db_action();
    $_SESSION['user_email'] = $_POST['user_email'];
    header('location: http://test.local');
}

function getdata() {

    $total_noq = get_total_no_of_question();
    $questions = get_questions();
    $data[0] = $total_noq;
    $data[1] = $questions;
    return $data;
}

function start_quiz($question_no) {

    // set time to 15 min for 3 questions
    set_time();
    

    if($question_no == 0) {

        //$_SESSION['current_question'] = $questions[0]['question_id'];
        // foreach($questions as $question) {
        //    $_SESSION['questions']= $question['question_id'];
        // } 
        $data = getdata();
        $total_noq = $data[0];
        $questions = $data[1];
        $_SESSION['questions'] = $questions;
        $count = count($questions);
        $_SESSION['questions_index'] = array();
        for($i = 0; $i < $count; $i++) {
            $flag = true;
            $check = rand(0,$count-1);
            for($j = 0; $j < $i; $j++) {
                if($_SESSION['questions_index'][$j] == $check) {
                    $flag = false;
                    break; 
                }
            }
            if($flag == true) {
                $_SESSION['questions_index'][$i] = $check;
            }
            else {
                $i = $i - 1; 
            }
        }
        $_SESSION['current_question'] = 0;
        $_SESSION['original_question_id'] = $_SESSION['questions'][$_SESSION['questions_index'][0]]['question_id'];
        display_question($_SESSION['questions'][$_SESSION['questions_index'][0]]);
    }
    elseif($question_no < count($_SESSION['questions'])) {
        $_SESSION['current_question'] = $question_no;
        $_SESSION['original_question_id'] = $_SESSION['questions'][$_SESSION['questions_index'][$question_no]]['question_id'];
        display_question($_SESSION['questions'][$_SESSION['questions_index'][$question_no]]);
    }
    else {
        display_final_score();
    }

    // // check whether 4question no is valid
    // foreach($questions as $question) {
    //     if($question_no == $question['question_id']) {
    //         $flag = true;
    //         break;
    //     }
    // }
    // elseif($flag == true) {
    //     $_SESSION['current_question'] = $question_no;
    //     display_question($questions[$question_no]);
    // }
    // else {
    //     display_final_score();
    // }
}

function display_question($q) {

    $question = $q;
    $options[] = $q['answer'];
    $options[] = $q['option1'];
    $options[] = $q['option2'];
    $options[] = $q['option3']; 
    require 'templates/display_question.tpl.php';
}

function submit_answer() {

    submit_answer_in_db();
    $question_no = $_SESSION['current_question'];
    $question_no++;
    $header_value = "Location: http://test.local/index.php/start_quiz?question_no=$question_no";
    header($header_value);
}

function display_final_score() {
    
    $score = get_final_score();
    require 'templates/score.tpl.php';
}
?>