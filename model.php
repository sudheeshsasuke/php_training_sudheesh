<?php

function open_db_connection() {
    
    $host = "localhost";
    $dbname = "php_training";
    $username = "root";
    $password = "root";
    $link = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    return $link;
}

function close_db_connection ($link) {

    $link = null;
}

function register_db_action() {

    $link = open_db_connection();

    //check whether the email already registered
    $query = "SELECT COUNT( * ) "
    . " FROM  `user`" 
    . " WHERE email = :email";
    $result = $link->prepare($query);
    $result->bindParam(':email', $_POST['user_email']);
    $t = $result->execute();
    $row = $result->fetch(PDO::FETCH_ASSOC);

    if($row['COUNT( * )'] > 0) {

        // set score of user to zero
        $score = 0;
        $query = "UPDATE `user` "
        . " SET `score`= :score "
        . " WHERE `email` = :email";
        $result = $link->prepare($query);
        $result->bindParam(':email', $_POST['user_email']);
        $result->bindParam(':score', $score);
        $t = $result->execute();
        echo "<h2>This email has already registered</h2>";
    }
    else{
        $query = "INSERT INTO `user`(`name`, `email`) "
        . " VALUES(:name, :email)";
        $result = $link->prepare($query);
        $result->bindParam(':name', $_POST['user_name']);
        $result->bindParam(':email', $_POST['user_email']);
        $t = $result->execute();
    }
}

function set_time() {

}

function get_total_no_of_question() {

    // get total no of questions
    $link = open_db_connection();
    $id = 1;
    $query = "SELECT * FROM  `total`"
     . " WHERE id =  :id";
    $result = $link->prepare($query);
    $result->bindParam(':id', $id);
    $t = $result->execute();
    $row = $result->fetch(PDO::FETCH_ASSOC); 
    $total_noq = $row['total_noq'];
    close_db_connection($link);
    return $total_noq;
}

function get_questions() {

    // TODO get no of questions from each category

    // for now get all categories and its get_questions
    $link = open_db_connection();
    $query = "SELECT *, cq.id AS question_id"
    . " FROM  `category_questions` AS cq"
    . " JOIN  `category` AS c ON cq.cat_id = c.id";
    $result = $link->query($query);
    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $questions[] = $row;
    }; 

    $query2 = "SELECT * FROM `category`";
    $result2 = $link->query($query2);
    while($row2 = $result2->fetch(PDO::FETCH_ASSOC)) {
        $categories[] = $row2;
    };

    $query3 = "SELECT * FROM `total`";
    $result3 = $link->query($query3);
    while($row3 = $result3->fetch(PDO::FETCH_ASSOC)) {
        $totals[] = $row3;
    };

    // Total questions are in $questions
    // shuffle questions
    $count = count($questions);
    $questions2 = array();
    for($i = 0; $i < $count; $i++) {
        $flag = true;
        $check = rand(0,$count-1);
        for($j = 0; $j < $i; $j++) {
            if($questions2[$j] == $check) {
                $flag = false;
                break; 
            }
        }
        if($flag == true) {
            $questions2[$i] = $check;
        }
        else {
            $i = $i - 1; 
        }
    }

    // take each category questions in $cat_que
    foreach($questions2 as $index) {
        foreach ($categories as $c) {
            if($questions[$index]['cat_id'] == $c['id']) {
                $cat_que[$c['id']][] = $questions[$index];
            }
        }
    }

    foreach($categories as $c) {
                    
        // calculate amount of questions fetched from each category
        $q_from_each_cat = round(($c['per_noq'] * $totals[0]['total_noq']) / 100);
        for($i = 0; $i < $q_from_each_cat; $i++) {
            $quiz_questions[] = $cat_que[$c['id']][$i];
        }
    }

    close_db_connection($link);
    return $quiz_questions;
}

function get_correct_answer($a) {

    $link = open_db_connection();
    $query = "SELECT * FROM  `category_questions`"
    . " where id = :id";
    $result = $link->prepare($query);
    $result->bindParam(':id', $a);
    $t = $result->execute();
    $row = $result->fetch(PDO::FETCH_ASSOC); 
    $answer= $row['answer'];
    close_db_connection($link);
    return $answer;
}


function upgrade_score() {

    $link = open_db_connection();

    //get current get_final_score
    $score = get_final_score();

    $query = "UPDATE `user` "
    . " SET `score`=:score WHERE `email`=:email";
    $result = $link->prepare($query);
    $score += 10;
    $result->bindParam(':score', $score);
    $result->bindParam(':email', $_SESSION['user_email']);
    $t = $result->execute();

}

function submit_answer_in_db() {
    
    $user_answer = $_POST['answer'];
    $correct_answer = get_correct_answer($_SESSION['original_question_id']);
    if($user_answer == $correct_answer) {

        //upgrade score
        upgrade_score();
    }
}

function get_final_score() {

    $link = open_db_connection();
    $query = "SELECT `score` FROM `user` "
    . " WHERE email = :email";
    $result = $link->prepare($query);
    $result->bindParam(':email', $_SESSION['user_email']);
    $t = $result->execute();
    $row = $result->fetch(PDO::FETCH_ASSOC); 
    $answer= $row['score'];
    close_db_connection($link);
    return $answer;
}
?>