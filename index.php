<?php
session_start();

require_once 'model.php';
require_once 'controller.php';

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
if($uri == '/') {
    if(!isset($_SESSION['user_email'])) {
        user_registartion();
    }
    else {
        require 'templates/start_quiz.tpl.php';
    }
}
elseif($uri == "/admin") { // admin side login

}
elseif($uri == "/index.php/register_user") {
    register_action();
}
elseif($uri == "/index.php/start_quiz" && isset($_GET['question_no'])) {
    start_quiz($_GET['question_no']);
}
elseif($uri == "/index.php/submit_answer") {
    
    //echo $_POST['answer'];
    submit_answer();
}
elseif($uri == "/index.php/logout") {
    unset($_SESSION['user_email']);
    $_SESSION = [];
    session_destroy();
    header('location: http://test.local');
}
else {
    echo "<h1>OOPS! ERROR!</h2>";
}
?>